// Import React
import React from "react";

// Import Spectacle Core tags
import {
    BlockQuote,
    Cite,
    Deck,
    Heading,
    ListItem,
    List,
    Quote,
    Slide,
    Text,
    Appear,
    Link,
    Image
} from "spectacle";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");


const images = {
    sprint1: require("../assets/sprint1.png")
};

preloader(images);

const theme = createTheme({
    primary: "white",
    secondary: "#1F2022",
    tertiary: "#03A9FC",
    quartenary: "#CECECE"
}, {
    primary: "Montserrat",
    secondary: "Helvetica"
});

export default class Presentation extends React.Component {
    render() {
        return (
            <Deck transition={["zoom", "slide"]} transitionDuration={500} theme={theme}>
                <Slide transition={["zoom"]} bgColor="primary">
                    <Heading size={1} fit caps lineHeight={1} textColor="secondary">
                        Inteligentny system sterowania natężeniem i barwą światła <br/>w pomieszczeniach o zmiennej intensywności oświetlenia naturalnego
                    </Heading>
                    <Text margin="10px 0 0" textColor="tertiary" size={1} fit bold>
                        Smart Lightning System
                    </Text>
                </Slide>
                <Slide transition={["fade"]} bgColor="tertiary">
                    <Heading size={1} textColor="secondary">Sprint 1</Heading>
                    <br/>
                    <Heading size={4} fit textColor="primary" >06.03.2017-19.03.2017</Heading>
                </Slide>
                <Slide>
                    <Heading size={2} textColor="secondary">Ukończono</Heading>
                    <br/>
                    <Heading size={1} textColor="red" caps fit>15 User Stories</Heading>
                </Slide>
                <Slide>
                    <Heading size={2} textColor="secondary">Zrealizowano</Heading>
                    <Text textColor="blue" fit>23 zadania</Text>
                </Slide>
                <Slide transition={["slide"]} bgColor="primary" textColor="tertiary">
                    <Heading fit textColor="secondary">Konfiguracja środowiska</Heading>
                    <List>
                        <ListItem>Skonfigurowano Bitbucket</ListItem>
                        <Appear><ListItem>Skonfigurowano platformę Taiga</ListItem></Appear>
                        <Appear><ListItem>Skonfigurowano komunikator Slack</ListItem></Appear>
                        <Appear><ListItem>Zintegrowano systemy wspomagające zarządzanie projektem</ListItem></Appear>
                    </List>
                </Slide>
                <Slide transition={["zoom"]} bgImage={images.sprint1}>
                    &nbsp;
                </Slide>
                <Slide transition={["zoom"]}>
                    <Heading fit>Ustalenie celów</Heading>
                    <List>
                        <ListItem>Zdefiniowano cele systemu</ListItem>
                        <Appear><ListItem>Opracowano możliwe zastosowania</ListItem></Appear>
                        <Appear><ListItem>Opisano zakres projektu</ListItem></Appear>
                        <Appear><ListItem>Przygotowano szkicowy opis wymagań</ListItem></Appear>
                        <Appear><ListItem>Opracowano ogólny model systemu</ListItem></Appear>
                        <Appear><ListItem>Opisano proponowane rozwiązanie</ListItem></Appear>
                        <Appear><ListItem>Oszacowano ryzyko niepowodzenia</ListItem></Appear>
                        <Appear><ListItem>Utworzono kosztorys</ListItem></Appear>
                        <Appear><ListItem>Sprawdzono możliwości integracji z zewnętrznymi systemami</ListItem></Appear>
                    </List>
                </Slide>
                <Slide transition="{[fade]}">
                    <Heading fit>Konfiguracja środowiska</Heading>
                    <List>
                        <ListItem>Wybrano źródła światła kolorowego</ListItem>
                        <Appear><ListItem>Wybrano wspierane czujniki</ListItem></Appear>
                        <Appear><ListItem>Dodano instrukcje umożliwiające rozpoczęcie pracy z Arduino</ListItem></Appear>
                        <Appear><ListItem>Dodano linki do pobrania wymaganego oprogramowania</ListItem></Appear>
                    </List>
                </Slide>
                <Slide transition="{[fade, zoom]}" notes="Ukończyliśmy pierwszy sprint i przygotowaliśmy zadania na najbliższy tydzień.">
                    <Heading fit caps>Co dalej?</Heading>
                </Slide>
                <Slide transition="{[slide, zoom]}" bgColor="secondary" notes="Część zespołu zapozna się z możliwościami wybranych przez nas komponentów oraz przygotuje proste aplikacje demonstracyjne w celu wczesnego wykrycia ewentualnych trudności">
                    <Heading fit textColor="green">Rozpoznanie możliwości komponentów<br/>oraz poznanie platformy Arduino</Heading>
                </Slide>
                <Slide transition="{[fade, slide]}" notes="Zaś druga część zespołu zajmie się poznawaniem platformy Android oraz możliwości tworzenia oprogramowania na tę platformę">
                    <Heading fit textColor="green">Poznanie platformy Android</Heading>
                </Slide>
                <Slide transition="{[fade, slide]}" bgColor="secondary">
                    <BlockQuote>
                        <Quote>
                            Dziękujemy :)
                        </Quote>
                    </BlockQuote>
                    <br/><br/>
                    <Link textColor="green" href="https://tree.taiga.io/project/natanielcz-smart-lightning-system/issues">Zgłoś sugestie</Link>
                </Slide>
            </Deck>
        );
    }
}
