// Import React
import React from "react";

// Import Spectacle Core tags
import {
    BlockQuote,
    Cite,
    Deck,
    Heading,
    ListItem,
    List,
    Quote,
    Slide,
    Text,
    Appear,
    Link,
    Image
} from "spectacle";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

import {PieChart, Pie, Legend, Sector, Cell, LineChart, AreaChart, XAxis, YAxis, Area} from "recharts";

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");



const theme = createTheme({
    primary: "white",
    secondary: "#1F2022",
    tertiary: "#03A9FC",
    quartenary: "#CECECE"
}, {
    primary: "Montserrat",
    secondary: "Helvetica"
});

const dataMsg = [{name: 'Direct', value: 4173}, {name: 'Channels', value: 1358}]
const dataStory = [
    {name: "Sprint 1", storyPoints: 40},
    {name: "Sprint 2", storyPoints: 24},
    {name: "Sprint 3", storyPoints: 36},
    {name: "Sprint 4", storyPoints: 30}
    ]
const COLORS = ["#03A9FC", "#CECECE"];

const MessagesPieChart = React.createClass({
    render () {
        return (
            <PieChart width={1000} height={400} >
                <Pie data={dataMsg}
                     innerRadius={100}
                     outerRadius={150}
                     fill="#8884d8"
                     cx="50%" cy="50%"
                     paddingAngle={5}>
                     {
                         dataMsg.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                     }
                     </Pie>
                <Legend/>
            </PieChart>
        );
    }
});

const StoryPointsOverTime = React.createClass({
    render() {
        return (
            <AreaChart width={1000} height={400} data={dataStory} margin={{top: 20, right: 100, left: 0, bottom: 20}}>
            <XAxis dataKey="name"/>
                <YAxis/>
                <Area type="monotone" dataKey="storyPoints" stroke={theme.secondary} fill={theme.tertiary}/>
            </AreaChart>
        );
    }
});


export default class Presentation extends React.Component {
    render() {
        return (
            <Deck transition={["zoom", "slide"]} transitionDuration={500} theme={theme}>
                <Slide transition={["zoom"]} bgColor="primary">
                    <Heading size={1} fit caps lineHeight={1} textColor="secondary">
                        Inteligentny system sterowania natężeniem i barwą światła <br/>w pomieszczeniach o zmiennej intensywności oświetlenia naturalnego
                    </Heading>
                    <Text margin="10px 0 0" textColor="tertiary" size={1} fit bold>
                        Smart Lightning System
                    </Text>
                </Slide>
                <Slide transition={["fade"]} bgColor="tertiary">
                    <Heading size={1} textColor="secondary">4 sprinty</Heading>
                </Slide>
                <Slide>
                    <Heading size={2} textColor="secondary">Ukończono</Heading>
                    <br/>
                    <Heading size={1} textColor="red" caps fit>33 User Stories</Heading>
                </Slide>
                <Slide>
                    <Heading size={2} textColor="secondary">Zrealizowano</Heading>
                    <Text textColor="blue" fit>130 zadań</Text>
                </Slide>
                <Slide transition={["slide"]} bgColor="secondary" textColor="tertiary">
                    <Heading caps fit>130 story pointów</Heading>
                </Slide>
                <Slide transition={["zoom"]} >
                    <StoryPointsOverTime/>
                </Slide>
                <Slide transition={["slide, fade"]} bgColor="tertiary">
                    <Heading fit textColor="primary">Komunikacja</Heading>
                </Slide>
                <Slide transition={["zoom"]}>
                    <MessagesPieChart />
                </Slide>
                <Slide transition={["slide"]}>
                    <Heading fit caps>15 miejsce</Heading>
                    <br/>
                    <Heading fit>w rankingu najaktywniejszych projektów w systemie Taiga</Heading>
                </Slide>
                <Slide transition="{[fade]}">
                    <Heading fit>Przetestowano ponad 15 komponentów</Heading>
                    <List>
                        <Appear><ListItem>Czujnik natężenia światła BH-1750</ListItem></Appear>
                        <Appear><ListItem>Klawiaturę membranową</ListItem></Appear>
                        <Appear><ListItem>Moduł WiFi ESP8266</ListItem></Appear>
                        <Appear><ListItem>Czujnik koloru TCS3200D</ListItem></Appear>
                    </List>
                </Slide>
                <Slide transition="{[zoom, slide]}" notes="" bgColor="secondary">
                    <Heading fit textColor="green">Arduino</Heading>
                </Slide>
                <Slide transition="{[zoom, slide]}" notes="" bgColor="secondary">
                    <Heading fit textColor="green">Android</Heading>
                </Slide>
                <Slide transition="{[zoom, slide]}" notes="" bgColor="secondary">
                    <Heading fit textColor="red">Web API</Heading>
                </Slide>
                <Slide transition="{[zoom, fade]}" notes="" bgColor="secondary">
                    <Heading fit caps textColor="white">Fail!</Heading>
                </Slide>
                <Slide transition="{[zoom, slide]}" notes="" bgColor="secondary">
                    <Heading fit textColor="red">Arduino != ESP8266</Heading>
                </Slide>
                <Slide transition="{[fade, slide]}" notes="">
                    <Heading fit caps textColor="green">Co dalej?</Heading>
                </Slide>
                <Slide transition="{[fade, slide]}" bgColor="secondary">
                    <BlockQuote>
                        <Quote>
                            Dziękujemy :)
                        </Quote>
                    </BlockQuote>
                    <br/><br/>
                    <Link textColor="green" href="https://tree.taiga.io/project/natanielcz-smart-lightning-system/issues">Zgłoś sugestie</Link>
                </Slide>
            </Deck>
        );
    }
}
