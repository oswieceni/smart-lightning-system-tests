var React = require('react');

module.exports = React.createClass({
    render: function() {
        return (
            <div id="lampadario" className={this.props.className}>
                <input type="radio" name="switch" value="on" checked={this.props.turnedOn} onChange={function(){}}/>
                <input type="radio" name="switch" value="off" checked={!this.props.turnedOn} onChange={function(){}}/>
                <label htmlFor="switch" id={this.props.id} className={this.props.color}></label>
                <div id="filo"></div>
                <div id="lampadina"></div>
            </div>
        );
    }
})