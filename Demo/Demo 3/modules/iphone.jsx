import React, {Component} from "react";
import {Player} from 'video-react';

export default class Phone extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            playerSource: require("../assets/final.mp4"),
            bulbColor: "red"
        }
    }

    componentDidMount() {
        this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
    }

    handleStateChange(state, prevState) {
        this.setState({
            player: state
        });

        let c = state.currentTime;

        let bulb = document.getElementById(this.props.id);
        if (c < 11) bulb.className = "none";

        // manual mode
        else if (c < 25) bulb.className = "white";
        else if (c < 28) bulb.className = "violet-pink";
        else if (c < 30) bulb.className = "orange";
        else if (c < 33) bulb.className = "orange-weak";
        else if (c < 41) bulb.className = "green-weak";

        // movement mode
        else if (c < 43) bulb.className = "none"; // activate move
        else if (c < 44) bulb.className = "green"; // drive
        else if (c < 46) bulb.className = "none"; // stop
        else if (c < 47.5) bulb.className = "green"; // drive

        // time mode
        else if (c < 64) bulb.className = "none"; // before
        else if (c < 67) bulb.className = "green"; // lights on

        // weather mode
        else if (c < 74) bulb.className = "none"; // before sun
        else if (c < 79) bulb.className = "red"; // sun
        else if (c < 83) bulb.className = "green"; // cloud
        else if (c < 87) bulb.className = "blue";

        // activity mode
        else if (c < 92) bulb.className = "none";
        else if (c < 119) bulb.className = "white";

        // flow mode

        // slow
        else if (c < 132) {
            if (Math.floor(c % 3) == 0) {
                if(!this.state.was) {
                    var newColor = "";
                    switch (this.state.bulbColor) {
                        case "red":
                            newColor = "green";
                            break;
                        case "green":
                            newColor = "blue";
                            break;
                        case "blue":
                            newColor = "red";
                            break;
                    }

                    this.setState({
                        bulbColor: newColor,
                        was: true
                    });

                    bulb.className = newColor;
                }
            } else {
                this.setState({
                    was: false
                });
            }
        }

        // fast
        else if (c < 148) {
            var newColor = "";
            switch(this.state.bulbColor) {
                case "red":
                    newColor = "green";
                    break;
                case "green":
                    newColor = "blue";
                    break;
                case "blue":
                    newColor = "red";
                    break;
            }

            this.setState({
                bulbColor: newColor
            });

            bulb.className = newColor;
        }

        // gesture mode
        else if (c < 150) bulb.className = "none";
        else if (c < 155) bulb.className = "green";
        else if (c < 160) bulb.className = "green-spring";
        else if (c < 165) bulb.className = "green-cyan";
        else if (c < 170) bulb.className = "green-spring";
        else if (c < 175) bulb.className = "none";
        else bulb.className = "green-spring";

        // headings
        let heading = document.getElementsByClassName("heading")[0];
        if (c < 23 || c > 180) heading.innerText = "";
        else if (c < 37) heading.innerText = "Profil Manualny";
        else if (c < 47) heading.innerText = "Profil Ruchu";
        else if (c < 63) heading.innerText = "Profil Czasu";
        else if (c < 92) heading.innerText = "Profil Pogodowy";
        else if (c < 117) heading.innerText = "Profil Aktywności";
        else if (c < 147) heading.innerText = "Profil Impreza";
        else heading.innerText = "Profil Gestów";

        // show clock
        let clock = document.getElementById("clock");
        if (c >= 50 && c <= 68) {
            clock.className = "stopwatch visible";
        } else {
            clock.className = "stopwatch hidden";
        }

        console.log(c);
    }

    render() {
        return (
            <div className="phone-shape">
                <span className="button-one buttons"></span>
                <span className="button-two buttons"></span>
                <span className="button-three buttons"></span>
                <span className="button-four button last"></span>
                <div className="top-details">
                    <span className="camera"></span>
                    <span className="circle"></span>
                    <span className="speaker"></span>
                </div>
                <div className="phone-screen">
                    <Player ref="player" autoPlay fluid={false} width={340} height={602} preload={"auto"}>
                        <source src={this.state.playerSource}/>
                    </Player>
                </div>
                <div className="circle-button"></div>
            </div>
        );
    }
}