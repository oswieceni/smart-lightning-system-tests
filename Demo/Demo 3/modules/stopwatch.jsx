import React, {Component} from 'react';

const formattedSeconds = (sec) =>
('0' + Math.floor(sec / 60)).slice(-2) +
':' +
('0' + sec % 60).slice(-2)


export default class Stopwatch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            secondsElapsed: 552,
            childVisible: true
        };
        this.incrementer = setInterval(() =>
            this.setState({
                secondsElapsed: this.state.secondsElapsed === 1439 ? 0 : this.state.secondsElapsed + 1
            }), 100);
    }

    render() {
        return (
            <div id={this.props.id} className="stopwatch">
                {this.state.childVisible ?
                    <h1 className="stopwatch-timer">{formattedSeconds(this.state.secondsElapsed)}</h1>
                    : null}
            </div>
        );
    }
}