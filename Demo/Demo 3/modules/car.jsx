import React, {Component} from 'react';

export default class Img extends Component {

    render() {
        return (
            <div className={this.props.className}>
                <img src={this.props.imgSrc} width="100%" height="auto" />
            </div>
        );
    }
}
