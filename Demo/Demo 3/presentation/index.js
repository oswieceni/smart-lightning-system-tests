// Import React
import React from "react";

// Import Spectacle
import {
    BlockQuote,
    Cite,
    Deck,
    Heading,
    ListItem,
    List,
    Quote,
    Slide,
    Text,
    Appear,
    Link,
    Image
} from "spectacle";
import preloader from "spectacle/lib/utils/preloader";
import createTheme from "spectacle/lib/themes/default";

import {PieChart, Pie, Legend, Sector, Cell, LineChart, AreaChart, XAxis, YAxis, Area} from "recharts";
import MovingRender from "../modules/car";

// Import Own Modules
var Bulb = require("../modules/lightbulb.jsx");
import Phone from "../modules/iphone";
import Img from "../modules/car";
import Stopwatch from "../modules/stopwatch";

// Require CSS
require("../modules/stopwatch.css");
require("../modules/lightbulb.css");
require("../modules/iphone.css");
require("./main.css");
require("normalize.css");
require("spectacle/lib/themes/default/index.css");

const images = {
    modules: require("../assets/modules.png"),
    brochure: require("../assets/brochure.png"),
    poster: require("../assets/poster.png"),
    car: require("../assets/car.png"),
    sun: require("../assets/sun.png"),
    cloud: require("../assets/cloud.png")
};

preloader(images);

const theme = createTheme({
    primary: "#333333",
    secondary: "#1F2022",
    tertiary: "#03A9FC",
    darkGreen: "#388e3c",
    lightGreen: "#4caf50",
    yellowGreen: "#cddc39",

    //colors from android app
    red: "#C0392B",
    green: "#37c759",
    blue: "#44bb5",

    redOrange: "#f06e3a",
    greenSpring: "#7bf198",
    blueViolet: "#5373ce",

    orange: "#d27459",
    greenCyan: "#83b08e",
    violet: "#813991",

    orangeYellow: "#f6a443",
    cyan: "#61e1c8",
    violetPink: "#e13b77",

    yellow: "#ebd46a",
    cyanBlue: "#27a6d8",
    pink: "#e557ab",
    white: "#ffffff"
}, {
    primary: "Montserrat",
    secondary: "Helvetica"
});



export default class Presentation extends React.Component {
    render() {
        return (
            <Deck transition={["zoom", "slide"]} transitionDuration={500} theme={theme} progress="bar" align="center">
                <Slide transition={["zoom"]} bgColor="primary" maxWidth={1280} maxHeight={800}>
                    <Bulb turnedOn={true} color="green" className="bulb-start"/>
                    <Heading size={1} fit caps lineHeight={1} textColor="white">
                        Adaptatywny system sterowania natężeniem i barwą światła
                    </Heading>
                    <Text margin="10px 0 0" textColor="darkGreen" fit bold>
                        Smart Lightning System
                    </Text>
                </Slide>
                <Slide transition="{[fade]}" maxWidth={1280} maxHeight={800}>
                    <Bulb turnedOn={true} color="green" id="bulb1"/>
                    <Phone id="bulb1"/>
                    <Heading size={1} textColor="green" className="heading"/>
                    <Stopwatch id="clock" ref="clock"/>
                    <Img imgSrc={images.car} className="movCar"/>
                    <Img imgSrc={images.sun} className="weaSun"/>
                    <Img imgSrc={images.cloud} className="weaCloud"/>
                </Slide>
            </Deck>
        );
    }
}

// const dataMsg = [{name: 'Direct', value: 4173}, {name: 'Channels', value: 1358}]
// const dataStory = [
//     {name: "Sprint 1", storyPoints: 40},
//     {name: "Sprint 2", storyPoints: 24},
//     {name: "Sprint 3", storyPoints: 36},
//     {name: "Sprint 4", storyPoints: 30}
// ]
// const COLORS = ["#03A9FC", "#CECECE"];
//
// const MessagesPieChart = React.createClass({
//     render () {
//         return (
//             <PieChart width={1000} height={400}>
//                 <Pie data={dataMsg}
//                      innerRadius={100}
//                      outerRadius={150}
//                      fill="#8884d8"
//                      cx="50%" cy="50%"
//                      paddingAngle={5}>
//                     {
//                         dataMsg.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
//                     }
//                 </Pie>
//                 <Legend/>
//             </PieChart>
//         );
//     }
// });
//
// const StoryPointsOverTime = React.createClass({
//     render() {
//         return (
//             <AreaChart width={1000} height={400} data={dataStory} margin={{top: 20, right: 100, left: 0, bottom: 20}}>
//                 <XAxis dataKey="name"/>
//                 <YAxis/>
//                 <Area type="monotone" dataKey="storyPoints" stroke={theme.secondary} fill={theme.tertiary}/>
//             </AreaChart>
//         );
//     }
// });
