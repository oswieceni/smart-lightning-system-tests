#include "Arduino.h"
#include <Keypad.h>

const byte ROWS = 4;
const byte COLS = 4;
char keys[ROWS][COLS] = {
	{'1','2','3' , 'A'},
	{'4','5','6' , 'B'},
	{'7','8','9' , 'C'},
	{'*','0','#' , 'D'}
	};
										//Piny cyfrowe
byte rowPins[ROWS] = {9, 8, 7, 6}; 		// Wyprowadzenia wierszy
byte colPins[COLS] = {5, 4, 3, 2}; 		// Wyprowadzenia kolumn
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

void setup() {
	Serial.begin(9600);
}

void loop(){
  char key = keypad.getKey();			// Pobranie znaku z klawiatury

		  if (key != NO_KEY){
		    Serial.println(key);
		  }
}
