#include "Arduino.h"

int czujnik = A7;   //pin analogowy A7 połączony z dłuższą nóżką fototranzystora

void setup() {
  Serial.begin(9600);        //inicjalizacja monitora szeregowego
  Serial.println("Test fototranzystora");
  pinMode(13, OUTPUT);      //pin 13 ustawiony jako wyjście - dioda
}

void loop() {
  int war = analogRead(czujnik);      //odczytanie wartości z A1
  Serial.print(war);                  //wyświetlenie jej na monitorze
  if (war > 400)
  {
    Serial.print("   Dioda zapalona");  //gdy wartość przekroczy pewien ustalony próg, wtedy dioda na pinie 13 zaświeci się
    digitalWrite(13, HIGH);
  }
  else
  {
    digitalWrite(13, LOW);
  }
  Serial.println("");
  delay(200);                         //opóźnienie między kolejnymi odczytami
}
