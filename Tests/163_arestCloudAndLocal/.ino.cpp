#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2017-05-29 23:56:46

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <aREST.h>
void setup(void) ;
void loop() ;
void callback(char* topic, byte* payload, unsigned int length) ;
String ipToString(IPAddress address) ;

#include "cloudAndLocal.ino"


#endif
