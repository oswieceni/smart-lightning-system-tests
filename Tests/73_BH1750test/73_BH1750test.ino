#include "Arduino.h"
#include <Wire.h>
#include <BH1750.h>

BH1750 bh1750;

void setup(){
  Serial.begin(9600);
  bh1750.begin();
}

void loop() {
  uint16_t lux = bh1750.readLightLevel();    //Pobranie wartości natężenia światła
  Serial.print("Wartość natężenia światła: ");
  Serial.print(lux);// Natężenie podane w jednostce luks
  Serial.println(" lx");
  delay(700);
}
