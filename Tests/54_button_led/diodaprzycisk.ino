// the setup function runs once when you press reset or power the board
const int buttonPin = 4;
const int ledPin = 3;

int pressCounter = 0;
int buttonState = 0;
int previousState = 0;

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(buttonPin, OUTPUT);
  pinMode(ledPin, INPUT);
}

// the loop function runs over and over again forever
void loop() {

  buttonState = digitalRead(buttonPin);

  if (buttonState != previousState) {
    //state has changed
    if (buttonState == HIGH) {
      //button has been pressed
      pressCounter++;
      
    }
  }

  previousState = buttonState;

  if (pressCounter % 3 == 0)
    analogWrite(ledPin, 0);
  if (pressCounter % 3 == 1)
    analogWrite(ledPin, 127);
  if (pressCounter % 3 == 2)
    analogWrite(ledPin, 255);
 
 
  
  
             
}
