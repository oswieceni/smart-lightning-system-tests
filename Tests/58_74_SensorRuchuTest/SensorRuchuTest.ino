
#include "Arduino.h"

int czujnik = 8;   //pin 8 po��czony z sygna�em z czujnika
int dioda = 10;		//pin 10 pod��czony do diody
void setup(){
  Serial.begin(9600);        //inicjalizacja monitora szeregowego
  pinMode(czujnik, INPUT);   //ustawienie pinu 8 Arduino jako wej�cie
  pinMode(dioda, OUTPUT);	//ustawienie pinu 10 Arduino jako wyj�cie

  digitalWrite(czujnik, LOW); //ustawienie czujnika w stan niski
  digitalWrite(dioda, LOW);		//ustawienie diody w stan niski
}

void loop(){
  int ruch = digitalRead(czujnik);      //odczytanie warto�ci z czujnika

  //stan wysoki oznacza wykrycie ruchu, stan niski - brak ruchu
  if(ruch == HIGH)        //wykrycie ruchu
  {
	  digitalWrite(dioda,HIGH);		//zapalenie diody

  }
  else  {		//brak ruchu
	  digitalWrite(dioda, LOW);		//zgaszenie diody
	  }

  delay(5000);                         //op�nienie mi�dzy kolejnymi odczytami
}
