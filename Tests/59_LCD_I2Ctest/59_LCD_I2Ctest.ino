#include "Arduino.h"
#include "Wire.h"
#include "LCD.h"
#include "LiquidCrystal_I2C.h"

#define I2C_ADDR    0x3F		//Definicja adresacji I2C
#define BACKLIGHT_PIN     3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

//Biblioteka NewliquidCrystal_1.3.4
LiquidCrystal_I2C	lcd(I2C_ADDR, En_pin, Rw_pin, Rs_pin, D4_pin, D5_pin, D6_pin, D7_pin);

void setup()
{
  lcd.begin (16,2);				//Określenie liczby kolumn i wierszy, tj. rodzaju wyświetlacza
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd.home(); 					// Ustawienie kursora na początku
  lcd.print("Hello World");
  lcd.setCursor ( 5, 1 );       // Ustawienie kursora w 5 kolumnie i drugim wierszu
  lcd.print("Test I2C");
}

void loop()
{
  lcd.setBacklight(LOW);      // Wyłączenie podświetlenia LCD
  delay(100);
  lcd.setBacklight(HIGH);     // Włączenie podświetlenia LCD
  delay(100);
}
