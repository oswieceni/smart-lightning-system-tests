// Wyświetlacz LCD bez przejściówki
#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal.h>

// Konfiguracja pinów wyświetlacza (pin Arduino-pin LCD):
// D7-RS, D6-E, D5-D4, D4-D5, D3-D6, D2-D7
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);

unsigned char i;

void setup()
{
// Deklaracja rozmiaru wyświetlacza, 16 kolumn i 2 wiersze
	lcd.begin(16, 2);
}

void loop()
{
for (i=0 ; i<=15 ; i++)
 {
	lcd.setCursor(0, 0); 		// Ustawienie kursora w 0 kolumnie i pierwszym wierszu
	lcd.print("Hello World");	// Wyświetlenie wiadomości na LCD
	lcd.setCursor(i, 1); 		// Ustawienie kursora w i-tej kolumnie i drugim wierszu
	lcd.print(char(126));      	// Wyświetlenie wiadomości (strzałka) na LCD
	delay(500);
	lcd.clear();       			// Czyszczenie wyświetlacza
 }
}
