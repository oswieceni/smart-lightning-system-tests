#include "Arduino.h"

#define S0 3
#define S1 4
#define S2 5
#define S3 6
#define sensorOut 2

int frequency = 0;

void setup() {

  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);

  // Setting frequency-scaling to 20%
  digitalWrite(S0,HIGH);
  digitalWrite(S1,LOW);

  Serial.begin(9600);
}

void loop() {

  // Setting RED filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,LOW);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  int R = frequency;
  // Printing the value on the serial monitor
  Serial.print("R=");//printing name
  Serial.println(int(R));//printing RED color frequency
  delay(100);

  // Setting GREEN filtered photodiodes to be read
  digitalWrite(S2,HIGH);
  digitalWrite(S3,HIGH);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  int G = frequency;

  // Printing the value on the serial monitor
  Serial.print("G=");//printing name
  Serial.println(int(G));//printing GREEN color frequency

  digitalWrite(S2,HIGH);
  digitalWrite(S3,LOW);
  delay(100);

  // Setting BLUE filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,HIGH);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  int B = frequency;
  // Printing the value on the serial monitor
  Serial.print("B=");//printing name
  Serial.println(int(B));//printing BLUE color frequency
  digitalWrite(S2,HIGH);
  digitalWrite(S3,LOW);
  delay(2000);

}
