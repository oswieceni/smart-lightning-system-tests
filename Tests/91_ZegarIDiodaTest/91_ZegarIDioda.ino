#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "Wire.h"
#include "LiquidCrystal_I2C.h"

//LCD
#define LCD_COLUMNS 16
#define LCD_ROWS 4
#define LCD_SDA_PIN D14
#define LCD_SCL_PIN D15
LiquidCrystal_I2C lcd(0x3F, LCD_COLUMNS, LCD_ROWS);


int led = D5;		// Ustawienie pinu diody
int hour, minute, second;
int GTM = 2; // Ustalenie strefy
int forHour = 8;	// Ustawienie od - do której godziny ma świecić się dioda
int forMinute = 00;
int upHour = 16;
int upMinute = 30;

char ssid[] = "nazwaSieci";
char pass[] = "haslo";
unsigned int localPort = 2390;
IPAddress timeServerIP; // time.nist.gov NTP serwer
const char* ntpServerName = "time.nist.gov";
const int NTP_PACKET_SIZE = 48;
byte packetBuffer[ NTP_PACKET_SIZE];
WiFiUDP udp;

void setup()
{
	Wire.begin(LCD_SDA_PIN, LCD_SCL_PIN);
  Serial.begin(115200);
  lcd.begin();
  lcd.backlight();
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
  lcd.setCursor(0,0);
  lcd.print("hello");
  // Logowanie do sieci WiFi
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  // Informacje o sieci, do której się podłączyliśmy
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(udp.localPort());
}

void PrintTime(int hour, int minute, int second)
{
	lcd.print(hour);
	lcd.print(":");
    if ( minute < 10 ) {
          //  Jeśli poniżej 10 to dopisuje 0
          lcd.print('0');
    }
	lcd.print(minute);
	lcd.print(":");
    if ( second < 10 ) {
      // Jeśli poniżej 10 to dopisuje 0
    	lcd.print('0');
    }
	lcd.print(second);
}

void loop()
{
  WiFi.hostByName(ntpServerName, timeServerIP);
  // Wysłanie pakietu NTP do serwera czasu
  sendNTPpacket(timeServerIP);
  delay(1000);

  // Informacja zwrotna
  int cb = udp.parsePacket();
  if (!cb) {
    Serial.println("No packet yet.");
  }
  else {
	  // Przetwarzanie uzyskanych informacji
    udp.read(packetBuffer, NTP_PACKET_SIZE);

    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    const unsigned long seventyYears = 2208988800UL;
    unsigned long epoch = secsSince1900 - seventyYears;

    // Obliczenie godziny, minuty i sekundy
    hour = GTM+(epoch  % 86400L) / 3600;
    minute = (epoch  % 3600) / 60;
    second = epoch % 60;

    // Wyświetlenie na serial porcie
    Serial.print("The time is ");
    Serial.print(hour);
    Serial.print(':');
    if ( minute < 10 ) {
          //  Jeśli poniżej 10 to dopisuje 0
          Serial.print('0');
    }
    Serial.print(minute);
    Serial.print(':');
    if ( second < 10 ) {
      // Jeśli poniżej 10 to dopisuje 0
      Serial.print('0');
    }
    Serial.println(second);
    lcd.setCursor(2,1);
    PrintTime(hour, minute, second);

  }
  // Zapalenie diody w ustawionych godzinach ( Patrz: zmienne globalne)
  if ( (hour >=forHour && minute >= forMinute) && hour <= upHour ){
      if (hour == upHour && minute > upMinute){
    	  digitalWrite(led, LOW);
      }
      else digitalWrite(led, HIGH);
  }
  else digitalWrite(led,LOW);
  delay(10000);
}

// Wysyłanie do serweru czasu
unsigned long sendNTPpacket(IPAddress& address)
{
  Serial.println("sending NTP packet...");
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  packetBuffer[0] = 0b11100011;
  packetBuffer[1] = 0;
  packetBuffer[2] = 6;
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  udp.beginPacket(address, 123);
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}
