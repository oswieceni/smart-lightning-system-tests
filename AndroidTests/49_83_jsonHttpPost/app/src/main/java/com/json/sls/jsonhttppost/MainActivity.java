package com.json.sls.jsonhttppost;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button)findViewById(R.id.sendButton);

        //Listener nasłuchujący przycisk Send
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void, Void, String>() {

                    String json = formatDataAsJSON();

                    //Akcja na kliknięcie
                    @Override
                    protected String doInBackground(Void... params) {
                        return getServerResponse(json, "https://jsonplaceholder.typicode.com/posts");
                    }

                    //Akcja wykonywana po doInBackground
                    @Override
                    protected void onPostExecute(String result) {
                        TextView text = (TextView)findViewById(R.id.textView);
                        text.setText(result);
                    }


                }.execute();
            }

            //Utworzenie obiektu JSON i konwersja do String
            private String formatDataAsJSON() {
                final JSONObject root = new JSONObject();
                try{
                    root.put("name", "sls");
                    root.put("description", "Smart Ligthning System");

                    JSONArray components = new JSONArray();

                    //Dodanie do obieku JSON kluczy i wartości parami
                    components.put("name");
                    components.put("id");
                    components.put("state");

                    root.put("components", components);
                    //zwrocenie obiektu JSON jako ciągu znakow
                    return root.toString();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            public final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            //Wysłanie zapytania na serwer i odebranie odpowiedzi
            private String getServerResponse(String json, String url) {
                RequestBody body = RequestBody.create(JSON, json);
                Request request = new Request.Builder().url(url).post(body).build();
                Response response = null;
                try {
                    response = client.newCall(request).execute();
                    return response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return "Couldn't get response";
            }
        });
    }

}
