package com.json.sls.jsonhttpget;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button)findViewById(R.id.buttonGet);

        //reakcja na kliknięcie - nowy widok
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void, Void, String>() {
                    //akcja
                    @Override
                    protected String doInBackground(Void... params) {

                        return getServerResponse("https://jsonplaceholder.typicode.com/posts/1");
                    }
                    //akcja wykonana na koniec
                    @Override
                    protected void onPostExecute(String result) {
                        //wyświetlenie odpowiedzi na ekranie
                        TextView text = (TextView)findViewById(R.id.textView);
                        text.setText(result);

                    }
                }.execute();
            }

            //Metoda wysyła zapytanie GET do serwera i zwraca odpowiedź
            private String getServerResponse(String url) {
                //klient Http
                OkHttpClient client = new OkHttpClient();

                //zapytanie
                Request request = new Request.Builder().url(url).build();

                //odpowiedź
                Response response = null;
                try {
                    //nasłuchiwanie odpowiedzi
                    response = client.newCall(request).execute();
                    return response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return "Something went wrong";


            }
        });
    }


}
