#Inteligentny system sterowania natężeniem i barwą światła w pomieszczeniach o zmiennej intensywności oświetlenia naturalnego#

W ramach projektu powinien zostać zrealizowany projekt sterujący oświetleniem w pomieszczeniu. Natężenie i barwa światła sztucznego doświetlającego pomieszczenie powinny ulegać zmianie w zależności od zmieniających się warunków oświetlającego to pomieszczenie światła naturalnego.
Projekt realizowany z wykorzystaniem mikro-kontrolera Arduino oraz czujnika natężenia światła np. TSL2561 – cyfrowy czujnik natężenia światła otoczenia I2C – moduł SparkFun.

Błędy można zgłaszać na stronie: [Smart Lightning System](https://tree.taiga.io/project/natanielcz-smart-lightning-system/ "Smart Lightning System - Taiga")


